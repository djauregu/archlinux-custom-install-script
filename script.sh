#!/bin/bash

# Automatic script for installation
# It is advisable to disable UEFI Secure Boot in the firmware setup manually before attempting to boot Arch Linux. 
# In the boot menu, press E and at the end of the line type: video=1920x1080
# wifi-menu, connect to a wifi
# Manual steps: set the internet connection, check the name of the network device, check the name of the hard drive
# Set up information

# Basic
hostname=diego-laptop

root_password=1123581321

timezone=Europe/Stockholm
locale=( en_US.UTF-8 )
mirror=( Sweden )
keymap=us

# Users
user=( diegx )
user_password=( 1123581321 )
group=( uucp )

# Software configuration
software="base-devel gnu-netcat vim ifplugd wget openssh bash-completion screen"
service=( sshd )

# Network
lan_dev=wlan0
eth_dhcp_client_dev=( ${lan_dev} )  # On a router setup, this is typically all ethernet WAN devices

# Partitions
hdd=/dev/sda

uefi=false
if [ -d "/sys/firmware/efi/efivars" ]; then
  uefi=true
  uefi_bad_impl=false
fi

for f in setup-chroot-*.sh; do
    [ -e "$f" ] && has_setup_chroot=1 || has_setup_chroot=0
    break
done

for f in setup-post-*.sh; do
    [ -e "$f" ] && has_setup_post=1 || has_setup_post=0
    break
done

for f in setup-user-*.sh; do
    [ -e "$f" ] && has_setup_user=1 || has_setup_user=0
    break
done

has_setup_root=$(( $has_setup_chroot || $has_setup_post ? 1 : 0))
has_setup=$(( $has_setup_root || $has_setup_user ? 1 : 0))


# Start execution

# Sync clock with the net
timedatectl set-ntp true

(echo x; echo z; echo Y; echo Y) | gdisk ${hdd}

if [ "$uefi" = true ]
then
  (echo n; echo ""; echo ""; echo +550M; echo EF00; echo w; echo Y) | gdisk ${hdd}
  hdd_esp=`lsblk ${hdd} -p -r -n | tail -n1 | cut -d ' ' -f 1`
  mkfs.fat -F32 ${hdd_esp}
else
  (echo n; echo ""; echo ""; echo +1M; echo EF02; echo w; echo Y) | gdisk ${hdd}
fi

(echo n; echo ""; echo ""; echo ""; echo 8300; echo w; echo Y) | gdisk ${hdd}
hdd_root=`lsblk ${hdd} -p -r -n | tail -n1 | cut -d ' ' -f 1`
(echo y) | mkfs.ext4 ${hdd_root}
mount ${hdd_root} /mnt

if [ "$uefi" = true ]
then
  mkdir /mnt/boot
  mount ${hdd_esp} /mnt/boot
fi

for i in "${mirror[@]}"; do
  grep -i -A 1 --no-group-separator $i /etc/pacman.d/mirrorlist >> mirrorlist
done
mv mirrorlist /etc/pacman.d/mirrorlist

pacman -Syy

pacstrap /mnt base linux linux-firmware netctl dhcpcd

genfstab -U /mnt >> /mnt/etc/fstab

mem_size=`awk '/MemTotal/ {print $2}' /proc/meminfo`
fallocate -l ${mem_size}k /mnt/var/swapfile
chmod 600 /mnt/var/swapfile
mkswap /mnt/var/swapfile
swapon /mnt/var/swapfile
echo -e "/var/swapfile\tnone\tswap\tdefaults\t0 0" >> /mnt/etc/fstab

cp {install-conf,install-chroot,install-post}.sh /mnt/root/
if [[ $has_setup == 1 ]]; then
  cp setup-*.sh /mnt/root/
fi

arch-chroot /mnt /root/install-chroot.sh

umount -R /mnt
reboot

# install-chroot.sh

echo ${hostname} > /etc/hostname
ln -fs /usr/share/zoneinfo/${timezone} /etc/localtime

for i in ${locale[@]}; do
  sed -i "s/^#$i/$i/g" /etc/locale.gen
done
locale-gen
echo "LANG=${locale[0]}" > /etc/locale.conf

echo "KEYMAP=${keymap}" > /etc/vconsole.conf

pacman -Syy

use_intel_ucode=false
if [ -n "`lscpu | grep Vendor | grep Intel`" ]
then
  pacman -S --noconfirm intel-ucode
  use_intel_ucode=true
fi

if [ "$uefi" = true ]
then
  hdd_root=`mount | grep ' / ' | cut -d ' ' -f 1`
  pacman -S --noconfirm efibootmgr
  efibootmgr | grep '^Boot0' | cut -c 5-8 | while read n; do efibootmgr -b "$n" -B; done
  if [ "$uefi_bad_impl" = false ]
  then
    if [ "$use_intel_ucode" = true ]
    then
      initrd_options="initrd=/intel-ucode.img initrd=/initramfs-linux.img"
    else
      initrd_options="initrd=/initramfs-linux.img"
    fi
    efibootmgr --disk ${hdd} --part 1 --create --gpt --label "Arch Linux" --loader /vmlinuz-linux --unicode "root=`blkid ${hdd_root} -o export | grep '^PARTUUID'` rw ${kernel_options}"
  else
    if [ "$use_intel_ucode" = true ]
    then
      initrd_options="initrd  /intel-ucode.img\ninitrd  /initramfs-linux.img\n"
    else
      initrd_options="initrd  /initramfs-linux.img\n"
    fi
    bootctl --path=/boot install
    echo -e "default  arch" > /boot/loader/loader.conf
    echo -e "title   Arch Linux\nlinux   /vmlinuz-linux\n${initrd_options}options root=`blkid ${hdd_root} -o export | grep '^PARTUUID'` rw" > /boot/loader/entries/arch.conf
  fi
else
  pacman -S --noconfirm grub
  grub-install --target=i386-pc --recheck ${hdd}
  grub-mkconfig -o /boot/grub/grub.cfg
fi

pacman -S --noconfirm ${software}

orphans=`pacman -Qtdq`
if [ ! "${orphans}" == "" ]; then
  pacman -Rns ${orphans} --noconfirm || true
fi

echo "alias ll='ls -l'" >> /etc/bash.bashrc

for (( i = 0; i < ${#eth_dhcp_client_dev[@]}; i++ )); do
  echo -e "Description='A basic dhcp ethernet connection'\nInterface=${eth_dhcp_client_dev[$i]}\nConnection=ethernet\nIP=dhcp\nForceConnect=yes" > /etc/netctl/${eth_dhcp_client_dev[$i]}-dhcp
  systemctl enable netctl-ifplugd@${eth_dhcp_client_dev[$i]}
done

useradd -m -g users -G wheel aur
echo "aur ALL=(ALL) NOPASSWD: ALL" | (EDITOR="tee -a" visudo) 
# TODO: This permission should be removed after installation!

for (( i = 0; i < ${#user[@]}; i++ )); do
  useradd -m -g users -s /bin/bash ${user[$i]}
  if [ ! "${group[$i]}" == "" ]; then
    usermod -G ${group[$i]} ${user[$i]}
  fi

  echo -e "${user_password[$i]}\n${user_password[$i]}" | (passwd ${user[$i]})
done

if [ ! "${service}" == "" ]; then
  for s in ${service[@]}; do
    systemctl enable $s
  done
fi

if [ ! "$group" == "" ]; then
  for i in "${group[@]}"; do
    IFS=',' read -a grs <<< "$i"
    for j in "${grs[@]}"; do
      if [ "$(grep $j /etc/group)" == "" ]; then
        groupadd $j
      fi
    done
  done
fi

if [[ $has_setup_chroot == 1 ]]; then
  for f in setup-chroot-*.sh; do
    su -c ./${f} -s /bin/bash root
    cd /root
  done
  rm setup-chroot-*.sh
fi

echo -e "[Unit]\nDescription=Automated install, post setup\nAfter=network-online.target\nRequires=network-online.target\n\n\n[Service]\nType=oneshot\nExecStart=/root/install-post.sh\nWorkingDirectory=/root\n\n[Install]\nWantedBy=multi-user.target" >> /etc/systemd/system/install-post.service

echo -e "WARNING: POST INSTALL IN PROGRESS.\n  The install-post.sh script is running. It will first wait for an active Internet connection. Then it will start running the selected setup scripts. To see the progress, run 'journalctl -u install-post -f'. The computer will be rebooted automatically when the installation is complete!" > /etc/motd

systemctl enable install-post.service

echo -e "${root_password}\n${root_password}" | (passwd)

rm install-chroot.sh && exit